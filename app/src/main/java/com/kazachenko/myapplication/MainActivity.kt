package com.kazachenko.myapplication

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material.*
import androidx.compose.runtime.LaunchedEffect
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.kazachenko.myapplication.screens.*
import com.kazachenko.myapplication.stores.MainActivityStore
import com.kazachenko.myapplication.ui.theme.MyApplicationTheme
import kotlinx.coroutines.flow.collect
import org.koin.android.ext.android.get

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val store = get<MainActivityStore>()
        setContent {
            val navController = rememberNavController()
            LaunchedEffect(true) {
                store.currentScreen.collect {
                    navController.navigate(it.name)
                }
            }
            MyApplicationTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    NavHost(
                        navController = navController,
                        startDestination = Screens.NewGameScreen.name
                    ) {
                        composable(Screens.CreateUsersScreen.name) { CreateUsersScreen() }
                        composable(Screens.NewGameScreen.name) { NewGameScreen() }
                        composable(Screens.GameScreen.name) { GameScreen() }
                    }
                }
            }
        }
    }
}

enum class Screens {
    CreateUsersScreen,
    NewGameScreen,
    GameScreen,

}
