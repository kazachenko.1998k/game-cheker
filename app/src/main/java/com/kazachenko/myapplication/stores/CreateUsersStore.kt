package com.kazachenko.myapplication.stores

import com.kazachenko.myapplication.Screens
import com.kazachenko.myapplication.model.Repository
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch

class CreateUsersStore(
    private val mainActivityStore: MainActivityStore,
    private val gameStore: GameStore,
    private val repository: Repository,
) : Store() {

    val error = MutableSharedFlow<Throwable>()

    var nameGame = String()
    val list = MutableStateFlow<List<String>>(listOf())

    fun create(user: String) {
        list.value = list.value.toMutableList().apply { add(user) }
    }

    fun start() {
        launch {
            val req = repository.startGame(nameGame, list.value)
            if (req.isSuccess) {
                list.value = listOf()
                gameStore.list.value = req.getOrThrow().playersQueue
                mainActivityStore.navigate(Screens.GameScreen)
            } else {
                error.emit(req.exceptionOrNull()!!)
            }
        }
    }

}