package com.kazachenko.myapplication.stores

import com.kazachenko.myapplication.Screens
import com.kazachenko.myapplication.model.Repository
import com.kazachenko.myapplication.model.models.User
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch

class GameStore(
    private val mainActivityStore: MainActivityStore,
    private val repository: Repository,
) : Store() {

    val error = MutableSharedFlow<Throwable>()

    val list = MutableStateFlow<List<User>>(listOf())

    fun cancel() {
        launch {
            val req = repository.cancelGame()
            if (req.isSuccess) {
                list.value = listOf()
                mainActivityStore.navigate(Screens.NewGameScreen)
            } else {
                error.emit(req.exceptionOrNull()!!)
            }
        }
    }

    fun reset() {
        launch {
            val req = repository.resetCount()
            if (req.isSuccess) {
                list.value = req.getOrNull()!!.playersQueue
            } else {
                error.emit(req.exceptionOrNull()!!)
            }
        }
    }

    fun addNewUser(name: String) {
        launch {
            val req = repository.addUser(name)
            if (req.isSuccess) {
                list.value = req.getOrNull()!!.playersQueue
            } else {
                error.emit(req.exceptionOrNull()!!)
            }
        }
    }

    fun addPoints(points: String) {
        launch {
            if (list.value.isNullOrEmpty() || points.toIntOrNull() == null) {
                return@launch error.emit(IllegalStateException("Users empty"))
            }
            val req = repository.addPoints(points.toInt())
            if (req.isSuccess) {
                list.value = req.getOrNull()!!.playersQueue
            } else {
                error.emit(req.exceptionOrNull()!!)
            }
        }
    }

}