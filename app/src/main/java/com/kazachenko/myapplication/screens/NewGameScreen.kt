package com.kazachenko.myapplication.screens

import android.widget.Toast
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState
import com.kazachenko.myapplication.stores.NewGameStore
import com.kazachenko.myapplication.ui.theme.paddingXL
import com.kazachenko.myapplication.ui.theme.paddingXS
import com.kazachenko.myapplication.ui.theme.paddingXXL
import com.kazachenko.myapplication.ui.theme.paddingXXXL
import kotlinx.coroutines.flow.collect
import org.koin.java.KoinJavaComponent.get


@Composable
fun NewGameScreen(store: NewGameStore = get(NewGameStore::class.java)) {
    val gameName = remember { mutableStateOf("") }
    val context = LocalContext.current
    val state = store.existsGames.collectAsState()
    LaunchedEffect(true) {
        store.updateGames()
        store.error.collect {
            Toast.makeText(context, it.message ?: "Error", Toast.LENGTH_SHORT).show()
        }
    }
    SwipeRefresh(
        state = rememberSwipeRefreshState(isRefreshing = false),
        onRefresh = { store.updateGames() }) {
        LazyColumn(
            modifier = Modifier
                .fillMaxSize()
                .padding(paddingXXXL),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            item {
                OutlinedTextField(
                    value = gameName.value,
                    onValueChange = { gameName.value = it },
                    label = {
                        Text(text = "Game name")
                    },
                    modifier = Modifier.fillMaxWidth()
                )
            }
            item { Spacer(modifier = Modifier.height(paddingXL)) }
            item {
                Row {
                    Button(
                        onClick = { store.createGame(gameName.value) },
                        modifier = Modifier.weight(1f),
                        enabled = gameName.value.isNotEmpty()
                    ) {
                        Text(text = "Create")
                    }
                }
            }
            item {
                Text(
                    modifier = Modifier
                        .padding(top = paddingXXL)
                        .fillMaxWidth(),
                    text = "Exists games:"
                )
            }
            items(state.value) {
                Game(it.name, it.playersQueue.size, onClick = { store.goToGame(it) })
            }
        }
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun Game(
    name: String,
    countUsers: Int,
    onClick: () -> Unit
) {
    Card(
        modifier = Modifier
            .padding(vertical = paddingXS)
            .fillMaxWidth(),
        onClick = onClick
    ) {
        Row(modifier = Modifier.padding(paddingXL)) {
            Text(text = name)
            Spacer(modifier = Modifier.weight(1f))
            Text(text = "Gamers: ")
            Text(text = countUsers.toString())
        }
    }
}