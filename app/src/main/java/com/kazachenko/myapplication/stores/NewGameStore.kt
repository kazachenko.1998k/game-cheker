package com.kazachenko.myapplication.stores

import android.util.Base64
import com.kazachenko.myapplication.Screens
import com.kazachenko.myapplication.model.Repository
import com.kazachenko.myapplication.model.models.Game
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch

class NewGameStore(
    private val mainActivityStore: MainActivityStore,
    private val createUsersStore: CreateUsersStore,
    private val gameStore: GameStore,
    private val repository: Repository,
) : Store() {

    val error = MutableSharedFlow<Throwable>()

    val existsGames = MutableStateFlow<List<Game>>(listOf())

    fun createGame(name: String) {
        createUsersStore.nameGame = name
        mainActivityStore.navigate(Screens.CreateUsersScreen)
    }

    fun goToGame(game: Game) {
        repository.id = game.id
        gameStore.list.value = game.playersQueue
        mainActivityStore.navigate(Screens.GameScreen)
    }

    fun updateGames() {
        launch {
            val req = repository.allGames()
            if (req.isSuccess) {
                existsGames.value = req.getOrThrow()
            } else {
                error.emit(req.exceptionOrNull()!!)
            }
        }
    }

}