package com.kazachenko.myapplication.model.models

data class Game(
    val id: String,
    val name: String,
    val playersQueue: List<User>,
)
