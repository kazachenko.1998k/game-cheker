package com.kazachenko.myapplication

import android.app.Application
import com.kazachenko.myapplication.di.DI
import org.koin.core.context.GlobalContext

class AppImpl : Application() {
    override fun onCreate() {
        super.onCreate()
        GlobalContext.startKoin {
            modules(DI)
        }
    }
}