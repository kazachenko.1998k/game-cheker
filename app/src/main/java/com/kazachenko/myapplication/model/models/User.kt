package com.kazachenko.myapplication.model.models

data class User(
    val name: String,
    val points: Int,
)
