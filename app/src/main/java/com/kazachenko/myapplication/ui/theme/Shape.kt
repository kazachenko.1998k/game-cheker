package com.kazachenko.myapplication.ui.theme

import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Shapes
import androidx.compose.ui.unit.dp

val paddingXS = 4.dp
val paddingS = 8.dp
val paddingM = 12.dp
val paddingXL = 16.dp
val paddingXXL = 24.dp
val paddingXXXL = 32.dp

val Shapes = Shapes(
    small = RoundedCornerShape(paddingS),
    medium = RoundedCornerShape(paddingM),
    large = RoundedCornerShape(paddingXL)
)