package com.kazachenko.myapplication.stores

import com.kazachenko.myapplication.Screens
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch

class MainActivityStore: Store() {

    val currentScreen = MutableSharedFlow<Screens>()

    val currentAuth = MutableStateFlow("")

    fun navigate(screens: Screens){
        launch {
            currentScreen.emit(screens)
        }
    }

}