package com.kazachenko.myapplication.screens

import android.widget.Toast
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import com.kazachenko.myapplication.R
import com.kazachenko.myapplication.stores.CreateUsersStore
import com.kazachenko.myapplication.ui.theme.paddingXL
import com.kazachenko.myapplication.ui.theme.paddingXS
import kotlinx.coroutines.flow.collect
import org.koin.java.KoinJavaComponent.get


@Composable
fun CreateUsersScreen(store: CreateUsersStore = get(CreateUsersStore::class.java)) {
    val state = store.list.collectAsState()
    val context = LocalContext.current
    val newUserText = remember {
        mutableStateOf("")
    }
    LaunchedEffect(true) {
        store.error.collect {
            Toast.makeText(context, it.message ?: "Error", Toast.LENGTH_SHORT).show()
        }
    }
    LazyColumn(
        modifier = Modifier
            .fillMaxSize()
            .padding(paddingXL),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        item {
            OutlinedTextField(
                modifier = Modifier
                    .fillMaxSize(),
                value = newUserText.value,
                onValueChange = { newUserText.value = it },
                label = {
                    Text(
                        text = "New user name"
                    )
                })
        }
        item {
            Row(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(vertical = paddingXL)
            ) {
                Button(
                    modifier = Modifier.weight(1f),
                    onClick = {
                        store.create(newUserText.value)
                        newUserText.value = ""
                    },
                    enabled = newUserText.value.isNotEmpty()
                ) {
                    Text(text = "Add")
                }
                Spacer(modifier = Modifier.width(paddingXL))
                Button(
                    modifier = Modifier.weight(1f),
                    onClick = {
                        store.start()
                    },
                    enabled = state.value.isNotEmpty()
                ) {
                    Text(text = "Start!")
                }
            }
        }
        items(state.value) {
            User(
                name = it,
            )
        }
    }
}

@Composable
fun User(
    name: String,
) {
    Card(
        modifier = Modifier
            .padding(vertical = paddingXS)
            .fillMaxWidth(),
    ) {
        Row(modifier = Modifier.padding(paddingXL)) {
            Text(text = name)
        }
    }
}