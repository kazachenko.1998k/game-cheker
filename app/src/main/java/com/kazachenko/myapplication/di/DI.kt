package com.kazachenko.myapplication.di

import com.kazachenko.myapplication.model.Repository
import com.kazachenko.myapplication.stores.*
import org.koin.dsl.module

val DI = module {
    single { MainActivityStore() }
    single { NewGameStore(get(), get(), get(), get()) }
    single { GameStore(get(), get()) }
    single { CreateUsersStore(get(), get(), get()) }
    single { Repository() }
}