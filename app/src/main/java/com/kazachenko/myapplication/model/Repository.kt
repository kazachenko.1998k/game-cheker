package com.kazachenko.myapplication.model

import com.kazachenko.myapplication.model.models.Game
import com.kazachenko.myapplication.model.models.User
import io.ktor.client.*
import io.ktor.client.engine.android.*
import io.ktor.client.features.json.*
import io.ktor.client.features.logging.*
import io.ktor.client.request.*

class Repository {
    private val BASE_URL = "192.168.31.123"
    private val PORT = 8080
    var id: String = ""
    val client = HttpClient(Android) {
        install(JsonFeature) {
            serializer = GsonSerializer()
        }
        install(Logging) {
            logger = Logger.DEFAULT
            level = LogLevel.ALL
        }
    }

    suspend fun startGame(name: String, users: List<String>) = wrap {
        val res = client.post<Game>(
            port = PORT,
            host = BASE_URL,
            path = "game/start"
        ) {
            parameter("name", name)
            header("Content-Type", "application/json")
            body = users
        }
        id = res.id
        res
    }

    suspend fun allGames() = wrap {
        val res = client.get<List<Game>>(
            port = PORT,
            host = BASE_URL,
            path = "games"
        ) {
            header("Content-Type", "application/json")
        }
        res
    }

    suspend fun addUser(name: String) = wrap {
        client.put<Game>(
            port = PORT,
            host = BASE_URL,
            path = "game/$id/player"
        ) {
            parameter("name", name)
            header("Content-Type", "application/json")
        }
    }

    suspend fun getQueue() = wrap {
        client.get<Game>(
            port = PORT,
            host = BASE_URL,
            path = "game/$id"
        ) {
            header("Content-Type", "application/json")
        }
    }

    suspend fun addPoints(points: Int) = wrap {
        client.put<Game>(
            port = PORT,
            host = BASE_URL,
            path = "game/$id/turn"
        ) {
            parameter("points", points)
            header("Content-Type", "application/json")
        }
    }

    suspend fun resetCount() = wrap {
        client.put<Game>(
            port = PORT,
            host = BASE_URL,
            path = "game/$id/reset"
        ) {
            header("Content-Type", "application/json")
        }
    }


    suspend fun cancelGame() = wrap {
        client.delete<Unit>(
            port = PORT,
            host = BASE_URL,
            path = "game/$id/stop"
        ) {
            header("Content-Type", "application/json")
        }
    }

    private inline fun <T> wrap(action: () -> T): Result<T> =
        try {
            Result.success(action.invoke())
        } catch (ex: Exception) {
            Result.failure(ex)
        }

}