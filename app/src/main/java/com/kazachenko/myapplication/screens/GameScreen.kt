package com.kazachenko.myapplication.screens

import android.widget.Toast
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.input.KeyboardType
import com.kazachenko.myapplication.stores.GameStore
import com.kazachenko.myapplication.ui.theme.paddingXL
import com.kazachenko.myapplication.ui.theme.paddingXS
import kotlinx.coroutines.flow.collect
import org.koin.java.KoinJavaComponent.get


@Composable
fun GameScreen(store: GameStore = get(GameStore::class.java)) {
    val state = store.list.collectAsState()
    val context = LocalContext.current
    val newUserText = remember { mutableStateOf("") }
    val newPointsText = remember { mutableStateOf("0") }
    LaunchedEffect(true) {
        store.error.collect {
            Toast.makeText(context, it.message ?: "Error", Toast.LENGTH_SHORT).show()
        }
    }
    LazyColumn(
        modifier = Modifier
            .fillMaxSize()
            .padding(paddingXL),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        item {
            Row {
                Button(
                    modifier = Modifier.weight(1f),
                    onClick = { store.cancel() }
                ) {
                    Text(text = "Cancel")
                }
                Spacer(modifier = Modifier.width(paddingXL))
                Button(
                    modifier = Modifier.weight(1f),
                    onClick = { store.reset() }
                ) {
                    Text(text = "Reset")
                }

            }
        }
        item {
            Row(
                modifier = Modifier
                    .fillMaxSize(),
                verticalAlignment = Alignment.CenterVertically
            ) {
                OutlinedTextField(
                    modifier = Modifier.weight(1f),
                    value = newUserText.value,
                    onValueChange = { newUserText.value = it },
                    label = {
                        Text(
                            text = "New user name"
                        )
                    })
                IconButton(
                    onClick = {
                        store.addNewUser(newUserText.value)
                        newUserText.value = ""
                    },
                ) {
                    Icon(
                        painter = painterResource(id = android.R.drawable.ic_input_add),
                        contentDescription = null
                    )
                }
            }
        }
        item {
            Row(
                modifier = Modifier
                    .fillMaxSize(),
                verticalAlignment = Alignment.CenterVertically
            ) {
                OutlinedTextField(
                    modifier = Modifier.weight(1f),
                    value = newPointsText.value,
                    onValueChange = { newPointsText.value = it },
                    label = {
                        Text(
                            text = "Points"
                        )
                    },
                    keyboardOptions = KeyboardOptions.Default.copy(keyboardType = KeyboardType.NumberPassword)
                )
                IconButton(
                    onClick = {
                        store.addPoints(newPointsText.value)
                        newPointsText.value = ""
                    },
                ) {
                    Icon(
                        painter = painterResource(id = android.R.drawable.ic_input_add),
                        contentDescription = null
                    )
                }
            }
        }
        itemsIndexed(state.value) { index, item ->
            User(
                name = item.name,
                count = item.points,
                selected = index == 0
            )
        }
    }
}

@Composable
fun User(
    name: String,
    count: Int,
    selected: Boolean,
) {
    Card(
        modifier = Modifier
            .padding(vertical = paddingXS)
            .fillMaxWidth(),
        backgroundColor = if (selected) MaterialTheme.colors.primary else MaterialTheme.colors.surface
    ) {
        Row(modifier = Modifier.padding(paddingXL)) {
            Text(text = name)
            Spacer(modifier = Modifier.weight(1f))
            Text(text = count.toString())
        }
    }
}